part of 'main.dart';

const Map<String, String> organisasi = {
  "Baraya UI Sukabumi": "Oct 2019 - Present, Staf of Media",
  "Ristek Fakultas Ilmu Komputer Universitas Indonesia":
      "Mar 2020 - present, Junior Member of Mobile Development SIG"
};

const Map<String, List<dynamic>> professionalSkill = {
  "Flutter": [Color(0xffFFC01D), .025],
  "Adobe Illustrator": [Color(0xffF21C50), .05],
  "Mobile Development": [Color(0xff46D6F8), .015]
};

const Map<String, List<dynamic>> educationalEfficiency = {
  "Universitas Indonesia": [
    "Bachelor’s degree, Computer Science",
    "2019 - present"
  ],
};

const Map<String, List<dynamic>> license = {
  "Flutter & Dart The Complete Guide - Certificate of Completion": [
    "Udemy, Issued Jul 2020 - No Expiration Date",
    "Credential ID : UC-617ecf21-df84-4b46-9f1e-29a5a30f1835"
  ],
  "Introduction to Flutter Development Using Dart - Certificate of Completion":
      [
    "The App Brewery, Issued Apr 2020 - No Expiration Date",
    "Credential ID : cert_l299w8rz"
  ]
};

const Map<String, String> achievement = {
  "3rd Winner of Pesta Rakyat Fisika Science Project Competition":
      "2015 - Universitas Indonesia",
  "3rd Winner of Junior Class Chess Competition ":
      "2017 - Universitas Muhammadiyah Sukabumi",
};
